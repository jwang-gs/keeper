# Keeper

Lighthouse automation tool. Takes CSV file of URLs and generates report files.

To run:

```
node app.js path_to_file.csv
```