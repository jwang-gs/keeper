const fs = require('fs').promises;
const lighthouse = require('lighthouse');
const throttling = require('lighthouse/lighthouse-core/config/constants').throttling;
const {computeMedianRun} = require('lighthouse/lighthouse-core/lib/median-run');
const ReportGenerator = require('lighthouse/lighthouse-core/report/report-generator');
const chromeLauncher = require('chrome-launcher');
const parse = require('csv-parse/lib/sync');

const siteFile = process.argv[2];

if (!siteFile) {
    console.error('CSV file required!');
} else {
    (async function() {
        // load CSV
        const urlFile = await fs.readFile(siteFile);
        const data = parse(urlFile);

        // Find address column header
        let address = data[0].indexOf('Address');
        let status = data[0].indexOf('Status Code');
        let content = data[0].indexOf('Content');
        if (address >= 0 && status >= 0  && content >= 0) {
            // launch headless browser and run lighthouse
            const chrome = await chromeLauncher.launch({chromeFlags: ['--headless']});
            const options = {
                output: 'json',
                onlyCategories: ['performance'],
                emulatedFormFactor: 'mobile',
                throttling: throttling.mobileSlow4G,
                port: chrome.port,
            };

            for (let row = 1; row < data.length; row++) {
                const url = data[row];
                // make sure URL points to a valid page
                if (url[status] === '200' && url[content].includes('text/html')) {
                    let results = [];
                    // generate 5 reports for a more accurate median score
                    for (let run = 0; run < 5; run++) {
                        console.log(`Performing run #${run + 1} for ${url[address]}`);
                        const lhResult = await lighthouse(url[address], options);
                        results.push(JSON.parse(lhResult.report));
                    }
                    const median = computeMedianRun(results);
                    console.log(`Median score for ${url[address]} is ${median.categories.performance.score * 100}`)

                    // take results from median report and output HTML
                    const reportHtml = ReportGenerator.generateReport(median, 'html');
                    const filename = url[address]
                        .replace('http://', '')
                        .replace('https://', '')
                        .replace(/\//g, '()');
                    await fs.writeFile(`./reports/${filename}.html`, reportHtml);
                    console.log(`Generated report for ${url[address]}`);
                } else {
                    console.log(`Invalid URL, skipping: ${url[address]}`);
                }
            }
            await chrome.kill();
            console.log("All reports completed.")
        } else {
            console.error('Missing one or more required headers: Address, Content, Status Code');
        }
    })();
}